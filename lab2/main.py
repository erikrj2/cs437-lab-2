import socket
import json
import psutil as ps
import picar_4wd as fc

SPEED = 100
DISTANCE_INC = 1
HOST = "192.168.1.16" # IP address of your Raspberry PI
PORT = 42070          # Port to listen on (non-privileged ports are > 1023)

def getDistTraveled(dir):
    global dist
    if dir["forward"] or dir["backward"]:
        dist = dist + DISTANCE_INC
    return dist

def drive(data):
    dir = "None"
    if data["left"]:
        fc.turn_left(SPEED)
        dir = "left"
    elif data["right"]:
        fc.turn_right(SPEED)
        dir = "right"
    elif data["forward"]:
        fc.forward(SPEED)
        dir = "forward"
    elif data["backward"]:
        fc.forward(-SPEED)
        dir = "backward"
    else:
        fc.stop()

    return dir

def getCPUTemp():
    #return ps.sensors_temperatures()['cpu-thermal'][0].current
    return ps.sensors_temperatures()["cpu_thermal"][0].current

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    global dist
    global ret_data
    dist = 0
    ret_data = {}
    s.bind((HOST, PORT))
    s.listen()
    counter = 0
    try:
        while 1:
            client, clientInfo = s.accept()
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            data = json.loads(data)
            dir = drive(data["key_status"])
            if (counter > 20):
                ret_data["cpu_temp"] = getCPUTemp()
                ret_data["distance"] = getDistTraveled(data["key_status"])
                ret_data["direction"] = dir
                ret_data["speed"] = 0 if dir == "None" else SPEED
            else:
                counter += 1

            client.sendall(json.dumps(ret_data).encode('utf-8')) # Echo back to client
    except Exception as e:
        print("Closing socket")
        print(e)
        client.close()
        s.close()
