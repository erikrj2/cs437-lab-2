document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 42070;
var server_addr = "192.168.1.16";   // the IP address of your Raspberry PI
var data = {
    "message": "",
    "key_status": {
        "forward": false,
        "backward": false,
        "left": false,
        "right": false
    }
}

function client(){
    const net = require('net');
    var input = document.getElementById("message").value;

    data["message"] = input;
    console.log(data)

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${JSON.stringify(data)}\r`);
    });
    
    // get the data from the server
    client.on('data', (data) => {
        // document.getElementById("bluetooth").innerHTML = data;
        var str = String.fromCharCode.apply(String, data);
        response = JSON.parse(str);
        document.getElementById("direction").innerHTML = response["direction"]
        document.getElementById("speed").innerHTML = response["speed"]
        document.getElementById("distance").innerHTML = response["distance"]
        document.getElementById("temperature").innerHTML = response["cpu_temp"]
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });


}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        data.key_status["forward"] = true;
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        data.key_status["backward"] = true;
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        data.key_status["left"] = true;
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        data.key_status["right"] = true;
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";

    data.key_status["forward"] = false;
    data.key_status["backward"] = false;
    data.key_status["left"] = false;
    data.key_status["right"] = false;
}


// update data for every 50ms
function update_data(){
    setInterval(function(){
        // get image from python server
        client();
    }, 100);
}
